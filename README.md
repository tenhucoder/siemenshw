# README #

This is Siemens HW 

### Requirement ###

node should be installed

### How to launch ###

1. Start node server 

* In the root folder 'cd hw-back-end'
* npm start

2. Start react app 
* In the root folder 'cd hw-front-end'
* npm start 
