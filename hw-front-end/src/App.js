import React from 'react';
import { Header } from './components/header/header';
import { Footer } from './components/footer/footer';
import Main from './containers/main/main';

function App() {
  return (
    <div id="container">
      <Header />
      <Main />
      <Footer />
    </div>
  );
}

export default App;
